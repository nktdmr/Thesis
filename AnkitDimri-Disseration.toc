\contentsline {chapter}{List of Figures}{ix}
\contentsline {chapter}{List of Tables}{xi}
\contentsline {chapter}{List of Symbols}{xii}
\contentsline {chapter}{Acknowledgments}{xvi}
\contentsline {chapter}{\numberline {1}Gas Turbine Theory and Motivation}{1}
\contentsline {section}{\numberline {1.1}Introduction}{1}
\contentsline {section}{\numberline {1.2}Applications}{3}
\contentsline {subsection}{\numberline {1.2.1}Aircraft Propulsion}{4}
\contentsline {subsection}{\numberline {1.2.2}Industrial Application}{6}
\contentsline {subsection}{\numberline {1.2.3}Marine and Land Transportation}{8}
\contentsline {section}{\numberline {1.3}Gas Turbine Design Process}{9}
\contentsline {subsection}{\numberline {1.3.1}CFD}{11}
\contentsline {subsection}{\numberline {1.3.2}Thermal}{13}
\contentsline {subsection}{\numberline {1.3.3}Mechanical}{14}
\contentsline {subsection}{\numberline {1.3.4}Lifing}{16}
\contentsline {subsection}{\numberline {1.3.5}Structure}{17}
\contentsline {subsection}{\numberline {1.3.6}Vibration}{19}
\contentsline {subsection}{\numberline {1.3.7}Cost}{21}
\contentsline {subsection}{\numberline {1.3.8}Manufacturing and Assembly}{22}
\contentsline {subsection}{\numberline {1.3.9}Design Process}{24}
\contentsline {subsection}{\numberline {1.3.10}Derivation and Combinations}{25}
\contentsline {section}{\numberline {1.4}Future of Gas Turbine}{27}
\contentsline {chapter}{\numberline {2}Operating Points and Engine Performance}{32}
\contentsline {chapter}{\numberline {3}Gas Turbine Thermodynamics and Performance}{33}
\contentsline {section}{\numberline {3.1}Introduction}{33}
\contentsline {section}{\numberline {3.2}Simple Turbojet Cycle}{35}
\contentsline {section}{\numberline {3.3}The Turbofan Cycle}{36}
\contentsline {chapter}{\numberline {4}Axial Gas Turbine Aerodynamics}{39}
\contentsline {section}{\numberline {4.1}Introduction}{39}
\contentsline {subsection}{\numberline {4.1.1}Basic Operation}{40}
\contentsline {section}{\numberline {4.2}Elementary Theory}{42}
\contentsline {section}{\numberline {4.3}Factors Affecting Stage Pressure Ratio}{45}
\contentsline {subsection}{\numberline {4.3.1}Tip Speed}{46}
\contentsline {subsection}{\numberline {4.3.2}Axial Velocity}{48}
\contentsline {subsection}{\numberline {4.3.3}High fluid deflections in rotor blades}{49}
\contentsline {section}{\numberline {4.4}Blockage in the compressor annulus}{52}
\contentsline {section}{\numberline {4.5}Degree of reaction}{55}
\contentsline {section}{\numberline {4.6}Meanline Analysis}{57}
\contentsline {section}{\numberline {4.7}Three Dimensional flow}{58}
\contentsline {subsection}{\numberline {4.7.1}Radial Equilibrium of fluid Element}{60}
\contentsline {subsection}{\numberline {4.7.2}Free Vortex}{62}
\contentsline {subsection}{\numberline {4.7.3}Exponential}{63}
\contentsline {subsection}{\numberline {4.7.4}First Power}{65}
\contentsline {section}{\numberline {4.8}Design Process}{66}
\contentsline {subsection}{\numberline {4.8.1}Determination of rotational speed and annulus dimensions}{68}
\contentsline {subsection}{\numberline {4.8.2}Estimation of number of stages}{70}
\contentsline {subsection}{\numberline {4.8.3}Stage by Stage Design}{71}
\contentsline {subsection}{\numberline {4.8.4}Variation of air angles from root to tip}{73}
\contentsline {section}{\numberline {4.9}Blade Design}{74}
\contentsline {subsection}{\numberline {4.9.1}Cascade Notation}{76}
\contentsline {subsection}{\numberline {4.9.2}Construction of blade shape}{78}
\contentsline {section}{\numberline {4.10}Calculation of stage performance}{79}
\contentsline {section}{\numberline {4.11}Off-Design performance}{81}
\contentsline {section}{\numberline {4.12}Axial compressor characteristics}{82}
\contentsline {chapter}{\numberline {5}Basic Airfoil Theory and Airfoil Selection}{85}
\contentsline {section}{\numberline {5.1}Introduction}{85}
\contentsline {section}{\numberline {5.2}Objective}{86}
\contentsline {section}{\numberline {5.3}Joukowsky Transformation}{86}
\contentsline {section}{\numberline {5.4}Inverse Design}{87}
\contentsline {chapter}{\numberline {6}Boundary Layer Interaction with Inviscid methods}{91}
\contentsline {section}{\numberline {6.1}Introduction}{93}
\contentsline {section}{\numberline {6.2}Panel Method}{94}
\contentsline {section}{\numberline {6.3}Euler's Method}{96}
\contentsline {section}{\numberline {6.4}Boundary Layer Interaction}{97}
\contentsline {section}{\numberline {6.5}MISES Results}{99}
\contentsline {section}{\numberline {6.6}Inlet Guide Vane}{101}
\contentsline {section}{\numberline {6.7}Intermediate Pressure Compressor}{109}
\contentsline {section}{\numberline {6.8}High Pressure Compressor}{124}
\contentsline {chapter}{\numberline {7}Turbine Annulus Design}{137}
\contentsline {section}{\numberline {7.1}Elementary Theory of Axial Flow Turbine}{137}
\contentsline {section}{\numberline {7.2}Vortex Theory}{139}
\contentsline {section}{\numberline {7.3}Choice of Blade Profile, Pitch and Chord}{140}
\contentsline {section}{\numberline {7.4}Estimation of Stage Performance}{142}
\contentsline {section}{\numberline {7.5}Overall Turbine Performance}{143}
\contentsline {section}{\numberline {7.6}The cooled Turbine}{145}
\contentsline {chapter}{\numberline {8}Integrated Gas Turbine Design}{147}
\contentsline {section}{\numberline {8.1}Elementary Theory of Axial Flow Turbine}{147}
\contentsline {section}{\numberline {8.2}Vortex Theory}{149}
\contentsline {section}{\numberline {8.3}Choice of Blade Profile, Pitch and Chord}{150}
\contentsline {section}{\numberline {8.4}Estimation of Stage Performance}{152}
\contentsline {section}{\numberline {8.5}Overall Turbine Performance}{153}
\contentsline {section}{\numberline {8.6}The cooled Turbine}{155}
\contentsline {chapter}{\numberline {9}Design of Experiments}{157}
\contentsline {section}{\numberline {9.1}Elementary Theory of Axial Flow Turbine}{157}
\contentsline {section}{\numberline {9.2}Vortex Theory}{159}
\contentsline {section}{\numberline {9.3}Choice of Blade Profile, Pitch and Chord}{160}
\contentsline {section}{\numberline {9.4}Estimation of Stage Performance}{162}
\contentsline {section}{\numberline {9.5}Overall Turbine Performance}{163}
\contentsline {section}{\numberline {9.6}The cooled Turbine}{165}
\contentsline {chapter}{\numberline {10}Conclusion}{168}
\contentsline {section}{\numberline {10.1}Elementary Theory of Axial Flow Turbine}{168}
\contentsline {section}{\numberline {10.2}Vortex Theory}{170}
\contentsline {section}{\numberline {10.3}Choice of Blade Profile, Pitch and Chord}{171}
\contentsline {section}{\numberline {10.4}Estimation of Stage Performance}{173}
\contentsline {section}{\numberline {10.5}Overall Turbine Performance}{174}
\contentsline {section}{\numberline {10.6}The cooled Turbine}{176}
\renewcommand {\cftchappresnum }{Appendix }
\contentsline {chapter}{\numberline {A}Gas Dynamics}{178}
\contentsline {section}{\numberline {A.1}Compressibility Effects}{178}
\contentsline {section}{\numberline {A.2}Basic Equation for Steady One-Dimensional Compressible Flow of a perfect gas in duct}{180}
\contentsline {section}{\numberline {A.3}Isentropic Flow}{181}
\contentsline {section}{\numberline {A.4}Normal Shock}{183}
\contentsline {section}{\numberline {A.5}Oblique Shock}{185}
\contentsline {section}{\numberline {A.6}Expansion Waves}{186}
\contentsline {section}{\numberline {A.7}Rayleigh Flow}{188}
\contentsline {section}{\numberline {A.8}Fanno Flow}{190}
\contentsline {chapter}{\numberline {B}Thin Airfoil Theory}{192}
\contentsline {section}{\numberline {B.1}Introduction}{192}
\contentsline {section}{\numberline {B.2}Elementary Flows}{194}
\contentsline {subsection}{\numberline {B.2.1}Uniform Flow}{195}
\contentsline {subsection}{\numberline {B.2.2}Source Flow}{196}
\contentsline {subsection}{\numberline {B.2.3}Doublets Flow}{199}
\contentsline {subsection}{\numberline {B.2.4}Vortex Flow}{201}
\contentsline {section}{\numberline {B.3}Methodology}{203}
\contentsline {section}{\numberline {B.4}Thin Airfoil Theory for NACA Airfoils}{205}
\contentsline {chapter}{Bibliography}{208}
