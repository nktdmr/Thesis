\chapter{Axial Gas Turbine Aerodynamics}
\textit{``When Frank Whittle told Ernest Hives that simplicity was a hallmark of his jet engine, the rolls royce director replied: 'We'll soon design the bloody simplicity out of it!' \\
	Of course, not once in the history of the jet engine has it been truly simple not in theory, not in manufacture, not in application ``}
\\[5pt]
\rightline{{\rm --- Rolls-Royce, The Jet Engine}}

\section{Introduction}
The importance of high overall pressure ratio in reducing specific fuel is significant, and obtaining high overall pressure ratio is itself difficult with centrifugal compressors. Axial flow compressor is used in applications where high overall pressure ratio and high specific fuel consumption is required. Another major advantage, especially for jet engines where higher mass flow rate is required for a given or constraint frontal area. Early axial flow compressors had pressure ratios of around 5:1 and required about 10 stages. Modern compressors of turbofan engines at Rolls-Royce have pressure ratios exceeding 50:1 and research is going on to further increase it to 65:1 and beyond. Continuous aerodynamics research and development has resulted steady improvement in stage pressure ratio. For applications like aircraft jet engine constraints like weight plays an important role in designing a compressor. At first it looks like adding more stages will increase pressure ratio, which is true but it comes with a very heavy cost of extra weight for an aircraft. Thus achieving maximum overall pressure ratio with limited stages/weight is one of the design challenge. We will try to solve this problem of optimal design later in our study. \par

Keeping it relevant to the preliminary design of gas turbine we will focus on subsonic compressors. True supersonic compressor, i.e. those for which the velocity at entry is everywhere supersonic, have not proceeded beyond experimental stage. However, Transonic compressors in which the velocity relative to a moving row of blades is supersonic over a part of the blade height, are now successfully used in modern gas turbines. 

\subsection{Basic Operation}

Compression of air is an unnatural activity. It has been likened to trying to sweep water uphill. In order to do its job, a modern compression system can require 200,000 HP- equivalent to the power of 250 formula one racing cars \cite{rollsroyce}. A compressor is a device that raises the pressure of the working fluid passing through it- in this case, air. A fan is a large, low-pressure, compressor found at the front of the most modern aero-engines.\\ For a modern large civil engine: \\
\begin{itemize}
	\item The fan passes over one tonne of airflow per second; this flow produces around 75 percent of the engine thrust\cite{rollsroyce}.
	\item Overall compression system pressure ratios are now approaching 50:1, and compressor exit temperatures can be over $700^{o} C$ \cite{rollsroyce}.
\end{itemize}
The design of the compression system is a complex inter-disciplinary task. Aerodynamics, noise, mechanics, manufacturing, and cost are all modeled during this process. The optimum configuration for each application is determined by performing a series of trade studies that consider all the leading attributes and requirements of the system, including life-cycle cost, weight, performance, and noise. As a preliminary design we will only focus on optimal aerodynamics design. \par

The axial flow compressor consist of a series of stages, each stage consist of a row of rotor blades followed by a row of stator blades. Individual stage can be seen in figure \ref{fig:ipcomp}. The working fluid is initially accelerated by the rotor blades, and then decelerated in the stator blade passage wherein the kinetic energy is transfered in the rotor is converted to static pressure. The process is repeated in as many stages as are necessary to yield the required overall pressure ratio.\par

The flow is always subject to an adverse pressure gradient, and the higher the pressure ratio the more difficult becomes the design of the compressor. The process consist of a series of diffusions, both in rotor and stator blade passage. Although the absolute velocity of fluid is increased in the rotor, it will be shown that the fluid relative to the rotor is decreased, i.e. there is diffusion within the rotor passages. The need for moderate rates of cross-sectional area in a diffusing flow is essential. This limit on the diffusion in each stage means that a single compressor stage can provide only a relatively small pressure ratio, and very much less than can be used by a turbine with its advantageous pressure gradient, converging blade passages, and accelerating flow as shown in figure \ref{fig:compcascade}. This is why a single turbine stage can drive a large number of compressor stages. \par

Careful design of compressor blading based on both aerodynamic theory and experiment is necessary, not only to prevent wasteful losses, but also to ensure minimum of stalling troubles which are all too prevalent in axial compressors, especially if the pressure ratio is high. Stalling, as in the case of isolated aerofoils, arises when the difference between the flow direction and the blade angle (i.e the angle of incidence) becomes excessive.   

% IP Compressor Figure from Rolls Royce Handbook
\begin{figure}[htb]
	\centering
	\includegraphics[width=0.8\linewidth]{Chapter-3/ipcomp1.png}
	\caption{8-Stage intermediate pressure ratio compressor, courtesy Rolls-Royce}
	\label{fig:ipcomp}
\end{figure}

The fact that pressure gradient is acting against the flow direction is always a danger to stability of the flow, and the flow reversals may easily occur at conditions of mass flow and rotational speed which are different from those for which the blades were designed. \par

The compressor shown in Fig. \ref{fig:ipcomp} makes use of inlet guide vanes (IGVs) which guides the flow into first stage. Variable IGVs are also used in most of the modern engines to allow to change flow inlet angles for different design points with different rotational speeds. This permits improved off-design performance. \par

Figure \ref{fig:ipcomp} shows the changed blade size from front to rear in an intermediate pressure ratio compressor. 


% Typical Compressor cascade figure
\begin{figure}[htb]
	\centering
	\begin{tikzpicture}
		\node (v1) at (-1.5,9) {};
		\node (v2) at (-1.5,1) {};
		\node (v3) at (2.5,9) {};
		\node (v4) at (2.5,1) {};
		\draw (v1) -- (v2);
		\draw (v3) -- (v4);
		\draw [densely dotted](-1.5,7) -- (2.5,5);
		\draw [densely dotted](2.5,2.5) -- (-1.5,4.5);
		\draw [semithick](-1.5,7);
		\draw [semithick](-1.5,7) .. controls (-1.5,8) and (0,5.5) .. (2.5,5);
		\draw [semithick](-1.5,7) node (v5) {} .. controls (-1.5,6) and (0,5) .. (2.5,5);
		\draw [semithick](-1.5,4.5) .. controls (-1.5,5.5) and (0,3) .. (2.5,2.5);
		\draw [semithick](-1.5,4.5) node (v6) {} .. controls (-1.5,3.5) and (0,2.5) .. (2.5,2.5);
		\draw [-latex](-3.5,7) -- (-2,6);
		\draw [-latex](3,3.5) -- (5,3);
		\node at (5,4) {$compressor$ $blades$};
		\draw [dash pattern=on 2pt off 3pt on 4pt off 4pt](v5) -- (-4.5,7) node (v8) {};
		\draw [dash pattern=on 2pt off 3pt on 4pt off 4pt](v6) -- (-4.5,4.5) node (v9) {};
		\node (v7) at (-4.5,5.5) {$pitch$};
		\draw [-latex](v7) -- (v8);
		\draw [-latex](v7) -- (v9);
		\node (v10) at (4,6) {$blade$ $direction$};
		\draw [-latex](v10) -- (4,7.5);
		\draw [-latex](4,4.5) -- (v10);
	\end{tikzpicture}
	\caption{compressor cascade}
	\label{fig:compcascade}
\end{figure}

\begin{figure}[htb]
	\centering
	\begin{tikzpicture}
		\draw [thin](-6.5,10) -- (-6.5,0);
		\draw [thin](-11,10) -- (-11,0);
		\draw [densely dotted](-11,7.5) -- (-6.5,4.5);
		\draw [densely dotted](-6.5,1.5) -- (-11,4.5);
		\draw [semithick](-11,4.5) .. controls (-11,5.5) and (-8,9.5) .. (-6.5,1.5);
		\draw [semithick](-11,4.5) node (v12) {} .. controls (-11,3.5) and (-8.5,6.5) .. (-6.5,1.5);
		\draw [semithick](-11,7.5) .. controls (-11,8.5) and (-8,12.5) .. (-6.5,4.5);
		\draw [semithick](-11,7.5) node (v11) {} .. controls (-11,6.5) and (-8.5,9.5) .. (-6.5,4.5) node (v17) {};
		\draw [dashdotdotted](v11) -- (-14,7.5) node (v14) {};
		\draw [dashdotdotted](v12) -- (-14,4.5) node (v15) {};
		\node (v13) at (-14,6) {$pitch$};
		\draw [-latex](v13) -- (v14);
		\draw [-latex](v13) -- (v15);
		\node at (-5,3.4) {$turbine$ $blades$};
		\draw (-6.87,4.4) node (v1) {} circle (0.369);
		\draw plot[smooth, tension=.7] coordinates {(v17) (-7.247,4.2981)};
		\draw [-latex](v1) -- (-7.247,4.2981);
		\draw [-latex] plot[smooth, tension=.7] coordinates {(v1) (v17)};
		\draw [semithick, -triangle 90](-3.5,1.5) -- (-3.5,7);
		\node at (-3.2,4.5) {$blade$ $direction$};
		\node (v2) at (-5.5,4) {$throat$};
		\draw [-latex](v2) -- (v1);
		\node at (-9.2,3.1) {$Chord$ $c$};
	\end{tikzpicture}
	\caption{turbine cascade}
\end{figure}

\section{Elementary Theory}
\Blindtext

\section{Factors Affecting Stage Pressure Ratio}
\Blindtext



\subsection{Tip Speed}
\Blindtext

\subsection{Axial Velocity}
\Blindtext

\subsection{High fluid deflections in rotor blades}
\Blindtext

\begin{figure}[htb]
	\centering
	\begin{tikzpicture}
		\draw (-5,3) -- (1,3);
		\draw (-5,4.5) -- (1,4.5);
		\draw (-5,7.5) -- (1,7.5);
		\draw (-5,0) -- (1,0);
		\draw [dotted](-3,7.5) -- (-4,4.5);
		\draw [dotted](-0.5,7.5) -- (-1.5,4.5);
		\draw [dotted](-4,3) -- (-3,0);
		\draw [dotted](-1.5,3) -- (-0.5,0);
		\draw [dotted](-3,7.5);
		\draw [semithick](-3,7.5) .. controls (-3.5,7.5) and (-4,7) .. (-4,4.5);
		\draw [semithick](-3,7.5) .. controls (-2.5,7.5) and (-3.5,7) .. (-4,4.5);
		\draw [semithick](-0.5,7.5) .. controls (-1,7.5) and (-1.5,7) .. (-1.5,4.5);
		\draw [semithick](-0.5,7.5) .. controls (0,7.5) and (-1,7) .. (-1.5,4.5);
		\draw [semithick](-4,3) .. controls (-3.5,3) and (-3,2.5) .. (-3,0);
		\draw [semithick](-4,3) .. controls (-4.5,3) and (-3.5,2.5) .. (-3,0);
		\draw [semithick](-1.5,3) .. controls (-1,3) and (-0.5,2.5) .. (-0.5,0);
		\draw [semithick](-1.5,3) .. controls (-2,3) and (-1,2.5) .. (-0.5,0);
		\draw [-triangle 90](-0.5,6) -- (2.5,6);
		\draw [-latex];
		\draw [-latex](-3,3.2) -- (-3,4.2) -- (-1.6,3.2);
		\draw [-latex](-2.9785,3.6856) arc (-88.3449:-35.1664:0.5019);
		\draw [-latex](-2.8,7.8) -- (-2.8,8.8) -- (-1.8,7.8);
		\draw [-latex](-2.7985,8.295) arc (-90.5994:-48.3109:0.4995);
		\draw  node (v1) {};
		\draw (v1);
		\draw (3,7.5) node (v2) {} -- (7.5,7.5) node (v5) {} -- (6,10) node (v3) {};
		\draw  plot[smooth, tension=.7] coordinates {(v2) (v3)};
		\draw  plot[smooth, tension=.7] coordinates {(6,10) (6,7.5)};
		\draw [-](6,10) node (v4) {};
		\draw [-latex](v4) -- (4.3035,8.5846);
		\draw [-latex](v4);
		\draw [-latex](v4) -- (6,8.6);
		\draw [-latex](v4);
		\draw [-latex](v4) -- (6.7644,8.6875);
		\draw [-](5.5425,9.6076) arc (-140.3093:-92.1055:0.5949);
		\draw [-](6.4,9.3072) arc (-59.9993:-91.6186:0.8);
		\node at (4.0549,8.8658) {$V_{1}$};
		\node at (5.59,9.2294) {$\beta_{1}$};
		\node at (6.2768,9.0678) {$\alpha_{1}$};
		\node at (6.1017,8.2733) {$C_{a1}$};
		\node at (7.1655,8.7042) {$C_1$};
		
		\draw  plot[smooth, tension=.7] coordinates {(3,3) (7.5,3)};
		\draw  plot[smooth, tension=.7] coordinates {(3,3)};
		\draw  plot[smooth, tension=.7] coordinates {(3,3) (4,5.5)};
		\draw  plot[smooth, tension=.7] coordinates {(4,5.5) (7.5,3)};
		\draw  plot[smooth, tension=.7] coordinates {(4,5.5) (4,3)};
		\draw (3.9958,4.8552) arc (-90.7192:-36.573:0.63);
		\draw (4.0037,4.5559) arc (-88.5062:-112.9321:0.9059);
		\node at (4.3933,4.7414) {$\alpha_2$};
		\node at (3.6952,4.1665) {$\beta_2$};
		\node at (4.1058,3.8175) {$C_{a2}$};
		\node at (6.2411,4.2281) {$C_2$};
		\node at (3.0382,4.0023) {$V_2$};
		\draw [dashdotted](v2) -- (3,3);
		\draw [dashdotted](v5) -- (7.5,3) node (v7) {};
		
		\node (v6) at (5.5,6.5) {$U$};
		\draw [-latex](v6) -- (7.5,6.5);
		\draw [-latex](v6) -- (3,6.5);
		\draw [dashdotted](4,3) -- (4,2.5) node (v10) {};
		\draw [dashdotted](v7) -- (7.5,2.5) node (v9) {};
		\node (v8) at (5.5,2.5) {$C_{W2}$};
		\draw [-latex](v8) -- (v9);
		\draw [-latex](v8) -- (v10);
		\draw [dashdotted](6,7.5) -- (6,7) node (v12) {};
		\node (v11) at (6.8,7) {$C_{W1}$};
		\draw [-latex](v11) -- (7.4,7);
		\draw [-latex](v11) -- (v12);
		\draw [-latex](4,5.4) -- (4,4);
		\draw [-latex](3.9986,5.504) node (v13) {} -- (3.4801,4.2226);
		\draw [-latex](v13) -- (5.7111,4.2631);
		\node at (-2.5084,8.0273) {$\alpha_1$};
		\node at (-1.9079,8.1955) {$C_1$};
		\node at (-2.6525,3.5116) {$\alpha_2$};
		\node at (-1.6917,3.5836) {$C_2$};
		\draw [-latex](-2.5,-2) -- (-2.5,-0.5) -- (-2,-2);
		\draw [-](-2.4897,-1.362) arc (-89.0134:-69.2613:0.888);
		\node at (-2.3,-1.5632) {$\alpha_3$};
		\node at (-1.871,-1.5632) {$C_3$};
		\node at (-5.9054,5.8724) {$Rotor$};
		\node at (-5.6788,1.3132) {$Stator$};
	\end{tikzpicture}
	\caption{velocity triangles at compressor cascade}
	
\end{figure}
\begin{figure}[htb]
	\centering
	\begin{tikzpicture}
		\draw (-6,1.5) -- (0,1.5) -- (5.5,1.5);
		\draw (-6,-2) -- (5.5,-2);
		\draw [dotted](-5.5,-2) -- (-2.5,1.5);
		\draw [dotted](5,1.5) -- (2,-2);
		\draw [dotted](-1.5,-2) -- (1,1.5);
		\draw [semithick](1,1.5) .. controls (0.5,1.5) and (-0.5,1) .. (-1.5,-2);
		\draw [semithick](1,1.5) .. controls (1.5,1.5) and (0.5,1) .. (-1.5,-2) node (v6) {};
		\draw [semithick](-2.5,1.5) .. controls (-3,1.5) and (-4,1) .. (-5.5,-2);
		\draw [semithick](-2.5,1.5) .. controls (-2,1.5) and (-3,1) .. (-5.5,-2);
		\draw [semithick](5,1.5) .. controls (4.5,1.5) and (3.5,1) .. (2,-2);
		\draw [semithick](5,1.5) node (v15) {} .. controls (5.5,1.5) and (4.5,1) .. (2,-2);
		\draw [thin](-5.8138,-2.9386) -- (-4.5108,0.7705);
		\draw [thin](-2.3696,1.4583) node (v1) {} .. controls (-2.6831,1.3114) and (-3.7902,0.3023) .. (-5.5046,-1.9999) node (v2) {};
		\draw [thin](1.0921,1.4439) node (v8) {} .. controls (0.689,1.1602) and (-0.4011,-0.1091) .. (-1.506,-2.0353) node (v7) {};
		\draw [thin](5.064,1.4439) node (v16) {} .. controls (4.6608,1.1751) and (3.2572,-0.2136) .. (2.0179,-2.0353) node (v9) {};
		\draw [thin](-1.2603,2.12) -- (-5.1731,-0.1511);
		\draw [dashed](v1) -- (-1.7656,0.9221) node (v4) {};
		\draw [dashed](v2) -- (-4.8024,-2.5486) node (v5) {};
		\node (v3) at (-3.3,-0.9112) {$c$};
		\draw [-latex](v3) -- (v4);
		\draw [-latex](v3) -- (v5);
		\draw (v6);
		\draw  plot[smooth, tension=.7] coordinates {(v6) (-1.5,0.5)};
		\draw  plot[smooth, tension=.7] coordinates {(-1.5,-3.5)};
		\draw  plot[smooth, tension=.7] coordinates {(v7) (-1.5,-3.5)};
		\draw (v8) -- (2.5,2.5);
		\draw (v8);
		\draw (1.1,1.5) -- (1.1,2.8);
		\draw [-latex](1.0951,2.3293) arc (89.7546:34.0348:0.8317);
		\draw [-latex](-1.4978,0.0602) arc (89.843:54.5931:2.0625);
		\draw (v6) -- (-2.0041,-3.4877);
		\draw  plot[smooth, tension=.7] coordinates {(v9) (2,-3.5)};
		\draw [-latex] plot[smooth, tension=.7] coordinates {(2,-2) (1,-3.5)};
		\draw [-] plot[smooth, tension=.7] coordinates {(2,-2) (1.7,-2.9)};
		\node (v10) at (0,-2.7) {$s$};
		\draw [-latex](v10) -- (2,-2.7);
		\draw [-latex](v10) -- (-1.5,-2.7);
		\draw [densely dashed](4.1,0.6) -- (4.8,0) node (v13) {};
		\draw [densely dashed](5.1,1.5) node (v14) {} -- (5.7,0.9) node (v12) {};
		\node (v11) at (5.243,0.4494) {$a$};
		\draw [-latex](v11) -- (v12);
		\draw [-latex](v11) -- (v13);
		\node at (5.3328,-0.3575) {$point$ $of$ $max$ $camber$ };
		\draw [-latex](7,3) -- (v14);
		\draw [-] plot[smooth, tension=.7] coordinates {(v15) (5,4)};
		\draw (v16) -- (6,3);
		\draw [-latex] (4.9887,3.6507) arc (90.301:35.8312:2.1507);
		\draw (5.5578,2.2604) arc (58.1416:37.3719:0.8927);
		\node at (5.7559,2.3527) {$i$};
		\draw [-latex](1.13,-2.3155) arc (-159.777:-124.1013:0.9163);
		\draw [-latex](2.4996,-2.6997) arc (-54.328:-108.2538:0.8627);
		\node at (1.3369,-2.453) {$\delta$};
		\draw [-latex](1.3032,-3.024) arc (-124.4615:-90.5457:1.2527);
		\node at (1.4757,-3.3065) {$\alpha_2$};
		\node at (0.7812,-3.3742) {$V_2$};
		\draw [-latex](-1.8607,-3.1038) arc (-108.1956:-90:1.1661);
		\node at (-1.7,-3.5) {$\alpha^{'}_2$};
		\draw [-latex](-4.533,0.6466) arc (70.4892:30.9199:0.5864);
		\node at (-4.2794,0.6732) {$\theta$};
		\node at (1.5101,2.4306) {$\alpha^{'}_1$};
		\node at (5.5988,3.7247) {$\alpha_1$};
		\node at (7.1525,3.0529) {$V_1$};
		\node at (-1.0781,0.2303) {$\zeta$};
	\end{tikzpicture}
	\caption{compressor cascade}
\end{figure}

\begin{figure}
		\centering
		\begin{tikzpicture}

			\draw (-5.5,2.5) -- (0,2.5) -- (5,2.5) node (v1) {};
			\draw (-5.5,-2.5) -- (5,-2.5) node (v2) {};
			\draw [densely dotted](-0.5,2.5) -- (-5,-2.5);
			\draw [densely dotted](4.5,2.5) -- (0.5,-2.5);
			\draw [thin] plot[smooth, tension=.7] coordinates {(v1) (8.5,2.5)};
			\draw [thin] plot[smooth, tension=.7] coordinates {(v2) (8.5,-2.5)};
			\draw [thin](4.5,2.5) .. controls (5,2.5) and (8.5,-1.5) .. (0.5,-2.5);
			\draw [thin](4.5,2.5) node (v5) {} .. controls (4,2.5) and (7,-0.5) .. (0.5,-2.5) node (v6) {};
			\draw [thin](-0.5,2.5) .. controls (0,2.5) and (3.5,-1.5) .. (-5,-2.5);
			\draw [thin](-0.5,2.5) .. controls (-1,2.5) and (2,-0.5) .. (-5,-2.5) node (v4) {};
			\draw [thin](-0.5,2.5);
			\draw [thin](-0.5,2.5) node (v3) {};
			\draw [thin] plot[smooth, tension=.7] coordinates {(v3)};
			\draw [thin] plot[smooth, tension=.7] coordinates {(v3) (0.1153,0.1519) (-1.83,-1.5898) (v4)};
			\draw [thin] plot[smooth, tension=.7] coordinates {(v5)};
			\draw [thin] plot[smooth, tension=.7] coordinates {(v5)};
			\draw [thin] plot[smooth, tension=.7] coordinates {(v5)};
			\draw [thin] plot[smooth, tension=.7] coordinates {(v5) (5.2574,0.1891) (3.6284,-1.5932) (v6)};
			\draw [thin](0.5,-2.5) node (v9) {} -- (-0.5,-1.2) node (v8) {};
			\draw [very thin] (0,-1.86) node (v7) {} circle (0.8001);
			\draw [-latex] plot[smooth, tension=.7] coordinates {(v7) (v8)};
			\draw [-latex] plot[smooth, tension=.7] coordinates {(0.0063,-1.8763) (v9)};
			\draw (v3) -- (-3,5) node (v11) {};
			\draw (v5) -- (2,5);
			\draw  plot[smooth, tension=.7] coordinates {(v3) (-0.5,5)};
			\draw  plot[smooth, tension=.7] coordinates {(v5) (4.5,5)};
			\draw  plot[smooth, tension=.7] coordinates {(-0.5,2.5) (-2,5)};
			\draw [-latex](-2.5,0.5) -- (0.5,1);
			\draw [-latex] plot[smooth, tension=.7] coordinates {(-4.3265,0.8632) (-1.1335,-0.6123)};
			\node (v10) at (-2.2675,-3.2918) {$opening$ $o$};
			\draw [-latex](v10) -- (-0.0349,-1.817);
			\node at (-3.8242,-0.9567) {$chord$ $c$};
			\draw [-latex](-2.3289,3.2012) arc (159.8641:135.4308:1.9634);
			\draw [-latex](-0.7723,4.4712) arc (98.3841:120.3432:1.9669);
			\node at (1.2,4.3892) {$angle$ $of$ $incidence $ $i$};
			\draw [-latex](-0.4916,4.8312) arc (89.9996:120.9847:2.3389);
			\node at (-1.1977,4.8851) {$\theta_2$};
			\draw [-latex](-3.4,5.4) -- (v11);
			\node at (-3,5.2) {$V_2$};
			\draw [dashed](v4) -- (-5,-3.6) node (v14) {};
			\draw [dashed](v9) -- (0.5,-3.6) node (v13) {};
			\node (v12) at (-2.3,-3.6) {$pitch$ $s$};
			\draw [-latex](v12) -- (v13);
			\draw [-latex](v12) -- (v14);
			\draw (v9) -- (0.5,-4);
			\draw  plot[smooth, tension=.7] coordinates {(v9) (-1.6,-3.9)};
			\draw (-0.479,-3.1408) arc (-146.3113:-89.1737:1.166);
			\node at (0.1743,-3.1456) {$\beta_3$};
			\node at (2.0,-3.1691) {$efflux$ $angle$};
			\node at (-3.031,-4.2645) {$\beta_3 \simeq cos^{-1} (o/s)$};
			\node at (-1.4369,0.8546) {$r_{upper}$};
			\node at (-3.168,-0) {$r_{lower}$};
		\end{tikzpicture}
	\caption{turbine cascade nomenclature}
\end{figure}

\begin{table}[h!]
	\centering
	\begin{tabular}{c c c c c}
		
		\hline 
		& & & & \\
		Engine & Date & Power[MW] & Pressure Ratio & Stages \\ [1.5ex]
		
		\hline
		
		& & & &\\
		
		TB 5000 & 1970 & 3.9 & 7.8 & 12\\ [.5ex]
		
		Tornado & 1981 & 6.75 & 12.3 & 15 \\ [.5ex]
		
		Typhoon & 1989 & 4.7 & 14.1 & 10 \\ [.5ex]
		
		Tempest & 1995 & 7.7 & 13.9 & 10 \\ [.5ex]
		
		Cyclone & 2000 & 12.9 & 16.9 & 11 \\ [.5ex]
		
		\hline
		
	\end{tabular}
	\caption{Different design laws}
	\label{table:Different_Engines}
\end{table}

\begin{table}[h!]
	\centering
	\begin{tabular}{c c c c c }
		
		\hline 
		& & & & \\
		Engine & Date & Thrust[KN] & Pressure Ratio & Stages \\ [1.5ex]
		
		\hline
		
		& & & &\\
		
		Avon & 1958 & 44 & 10 & 17\\ [.5ex]
		
		Spey & 1963 & 56 & 21 & 17 \\ [.5ex]
		
		RB-211 & 1972 & 225 & 29 & 14 \\ [.5ex]
		
		Trent & 1995 & 336 & 41 & 15 \\ [.5ex]
		
		GE90 & 2016 & 470 & 60 & 15 \\ 
		
		\hline
		
	\end{tabular}
	\caption{Engine thrust comparison over years}
	\label{table:thrust_comp}
\end{table}


\begin{table}[h!]
	\centering
	\begin{tabular}{c c c}
		
		\hline 
		& & \\ [.5ex]
		$n$ & $ \Lambda $ & Blading \\ [1.5ex]
		
		\hline
		
		& & \\
		
		-1 & $ 1 - \dfrac{1}{R^{2}} (1 - \Lambda_m)$ & Free Vortex \\ [2.5ex]
		
		0 & $ 1 + \left[ 1 - \dfrac{2}{R} \right] (1- \Lambda_m)$ & Exponential \\ [3.0ex]
		
		1 & $ 1 + (2 $ $\ln$ $ R - 1)(1 - \Lambda_m)$ & First Power \\ [2.5ex]
		
		\hline
				
	\end{tabular}
	\caption{Different design laws}
	\label{table:design_laws}
\end{table}

\section{Blockage in the compressor annulus}
\Blindtext

\section{Degree of reaction}
\Blindtext

\section{Meanline Analysis}
\Blindtext

\section{Three Dimensional flow}
\Blindtext

\subsection{Radial Equilibrium of fluid Element}
\Blindtext

\subsection{Free Vortex}
\Blindtext

\subsection{Exponential}
\Blindtext

\subsection{First Power}
\Blindtext


\section{Design Process}
\Blindtext

\subsection{Determination of rotational speed and annulus dimensions}
\Blindtext

\subsection{Estimation of number of stages}
\Blindtext

\subsection{Stage by Stage Design}
\Blindtext

\subsection{Variation of air angles from root to tip}
\Blindtext
Design Process of compressor design

\section{Blade Design}
\Blindtext

\subsection{Cascade Notation}
\Blindtext

\subsection{Construction of blade shape}
\Blindtext

How blade is stacked

\section{Calculation of stage performance}
\Blindtext

Calculation of stage performance

\section{Off-Design performance}
\Blindtext

Explain about off design performance and design

\section{Axial compressor characteristics}
\Blindtext

Text about chics of axial compressor



