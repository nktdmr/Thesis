%%% ======================================================================
%%%  @LaTeX-file{
%%%     filename  = "bitsthesis.sty",
%%%     version   = "2.3.2",
%%%     date      = "2007/06/06",
%%%     time      = "13:00:00 EDT",
%%%     author    = "Gary L. Gray & Francesco Costanzo",
%%%     copyright = "Gary L. Gray & Francesco Costanzo",
%%%     address   = "Engineering Science and Mechanics,
%%%                  212 Earth & Engineering Sciences Bldg.,
%%%                  Penn State University,
%%%                  University Park, PA 16802,
%%%                  USA",
%%%     telephone = "814-863-1778 (GLG) or
%%%                  814-863-2030 (FC)",
%%%     FAX       = "814-865-9974",
%%%     email     = "gray@engr.bits.edu (GLG) or
%%%                  costanzo@engr.bits.edu (FC)",
%%%     keywords  = "latex, bitsthesis, thesis class",
%%%     supported = "yes",
%%%     abstract  = "This package provides a style for typesetting
%%%                  Penn State theses at the bachelors, masters,
%%%                  or Ph.D. level."
%%%  }
%%% ======================================================================
% Change History
%
% Version 2.3.2: * Changed the implementation of the tocloft package
%                  to remove the ``black boxes'' about which we got so
%                  many queries. It still isn't perfect, but it is better
%                  than it was before. Note that chapter titles that take
%                  two lines or more in the TOC now have hanging indents.
%                  We haven't been able to eliminate this, but we kind of
%                  like it, so we are leaving it as is (for now).
%
% Version 2.3.1: * Added 10pt and 11pt options to the document class,
%                  though we have no idea why anyone would want to use
%                  such insanely small font sizes since it will lead to
%                  line lengths that are much too long.
%
% Version 2.3.0: * Made substantial changes to the bitsthesis.cls file
%                  since the draft option was not being passed to the book
%                  class. The change we made also prevents users from
%                  loading options that are not ``recommended''. We
%                  want to thank Jared Craig Ahern for pointing out this
%                  problem and suggesting the fix.
%
% Version 2.2.0: * Two additional class options have been added to
%                  support honors theses submitted to the Schreyer
%                  Honors College. These options are:
%                     - honors
%                     - honorsdepthead
%
%                * We have also added the commands:
%                     - honorsdegreeinfo
%                     - honorsadviser
%                     - honorsdepthead
%
%                  See the driver file for details on these options and
%                  commands.
%
% Version 2.1.2: * Added a note both here and in the template file for
%                  users wanted to implement the subfigure package. That
%                  is, if you want to use the subfigure package you need
%                  to add an option when the tocloft package is loaded.
%
% Version 2.1.1: * Fixed a bug, introduced in 2.1.0, that prevented any
%                  entries from appearing the List of Figures. :-)
%
% Version 2.1.0: * Class now requires the tocloft package to format the
%                  TOC to correspond to bits Grad School requirements.
%                * Added the \Appendix command to work around a quirk in
%                  LaTeX that makes \addtocontents not work well with
%                  \include.
%                * Eliminated a ton of booleans associated with the
%                  management of the frontmatter. This makes the
%                  frontmatter more intuitive, but it also presents you
%                  with the noose with which you can hang yourself. Sorry.
%                * Added the inlinechaptertoc option to the class. Allows
%                  for an alternate TOC format for Chapter and Appendix
%                  entries.
%                * There is no longer a sigpage option for the class.
%
% Version 2.0.5: * Updated the signature page and added a committee page
%                  to conform to the new grad school requirement.
%                * There is now a sigpage option for the signature page
%                  (it can be turned off) and the standard
%                  now does not have signatures.
%                * Fixed problem with TOC page numbers of List of Figures,
%                  List of Tables, etc.
%                * Cleaned up a few things, especially the boolean logic
%                  for the degree type.
%
%                Version 2.0.5 owes a debt to Penn State graduate student
%                Nils Krahnstoever who sent us a list of bugs along with
%                some excellent suggestions.
%
% Version 2.0.4: Changed the alignment of "Date of Signature" text
%
% Version 2.0.3: Fixed the invocation of \bits@title to allow for \\
%                line breaks. The \MakeUppercase command originally
%                was not, for some reason, working. Unfortunately, we
%                had to resort to some TeX primitives.
%
% Version 2.0.2: Added vita to bs and ba option for the Schreyer's
%                Honors College.
%
% Version 2.0.1: * No longer include the abstract in the TOC.
%                * Double space the title on the title page.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\NeedsTeXFormat{LaTeX2e}[1995/06/01]
\ProvidesClass{bitsthesis}[2007/06/06 v2.3.2 bitsthesis class]
\RequirePackage{ifthen}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Declare options for different degree types.
% Allowable degrees are:
%     Ph.D. using class option <phd>
%     M.S. using class option <ms>
%     M.Eng. using class option <meng>
%     M.A. using class option <ma>
%     B.S. using class option <bs>
%     B.A. using class option <ba>
%     Bachelors degree with honors using class option <honors>
%
% The option of an option sets the boolean for that option to
% true and all others to false.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newboolean{bits@secondthesissupervisor}
\newboolean{bits@honorsdepthead}
\newboolean{bits@honors}
\newboolean{bits@bs}
\newboolean{bits@ba}
\newboolean{bits@ms}
\newboolean{bits@meng}
\newboolean{bits@ma}
\newboolean{bits@phd}
\newboolean{bits@toc}
\setboolean{bits@secondthesissupervisor}{false}
\setboolean{bits@honorsdepthead}{false}
\setboolean{bits@honors}{false}
\setboolean{bits@bs}{false}
\setboolean{bits@ba}{false}
\setboolean{bits@ms}{false}
\setboolean{bits@meng}{false}
\setboolean{bits@ma}{false}
\setboolean{bits@phd}{false}

\DeclareOption{bs}{\setboolean{bits@bs}{true}\setboolean{bits@phd}{false}}
\DeclareOption{ba}{\setboolean{bits@ba}{true}\setboolean{bits@phd}{false}}
\DeclareOption{ms}{\setboolean{bits@ms}{true}\setboolean{bits@phd}{false}}
\DeclareOption{meng}{\setboolean{bits@meng}{true}\setboolean{bits@phd}{false}}
\DeclareOption{ma}{\setboolean{bits@ma}{true}\setboolean{bits@phd}{false}}
\DeclareOption{phd}{\setboolean{bits@phd}{true}}
\DeclareOption{inlinechaptertoc}{\setboolean{bits@toc}{true}}
\DeclareOption{honorsdepthead}{\setboolean{bits@honorsdepthead}{true}}
\DeclareOption{secondthesissupervisor}{\setboolean{bits@secondthesissupervisor}{true}}
\DeclareOption{honors}{\setboolean{bits@honors}{true}%...
\setboolean{bits@bs}{false}\setboolean{bits@ba}{false}\setboolean{bits@phd}{false}}

\DeclareOption{draft}{\PassOptionsToClass{\CurrentOption}{book}}
\DeclareOption{10pt}{\PassOptionsToClass{\CurrentOption}{book}}
\DeclareOption{11pt}{\PassOptionsToClass{\CurrentOption}{book}}
\DeclareOption{12pt}{\PassOptionsToClass{\CurrentOption}{book}}
\DeclareOption*{\PackageWarning{bitsthesis}{Unknown option `\CurrentOption'. Ignoring}}
\ExecuteOptions{phd} % the default option is <phd>
\ProcessOptions
\LoadClass[openany,twoside]{book}
\RequirePackage{calc}
\RequirePackage{setspace}
% If you are using the subfigure package, load the tocloft package with
% the subfigure option and comment out the next line.
\RequirePackage{tocloft}[2003/09/26]
%\RequirePackage[subfigure]{tocloft}[2003/09/26]
%\RequirePackage{fancyhdr}

% Define the margins and other spacings.
\setlength{\oddsidemargin}{0.6in}
\setlength{\topmargin}{0pt}
\setlength{\headheight}{12pt}
\setlength{\headsep}{1.8\baselineskip}
\setlength{\textheight}{8.60in}
\setlength{\textwidth}{5.8in}

% Set the baselinestretch using the setspace package.
% The LaTeX Companion claims that a \baselinestretch of
% 1.24 gives one-and-a-half line spacing, which is allowed
% by the bits thesis office. FC likes 1.28 better. :-)
\setstretch{1.28}


%%%%%%%%%%%%%%%%%%%%%%%%
% Settings for tocloft %
%%%%%%%%%%%%%%%%%%%%%%%%
% Format chapter entries so that the chapter name goes on a line
% following "Chapter #".
\renewcommand{\@pnumwidth}{1.75em} % remove TOC margin errors
\renewcommand{\@tocrmarg}{2.75em}
\newlength{\mylength}% a "scratch" length
\newlength{\mylonglength}% another "scratch" length
\ifthenelse{\boolean{bits@toc}}
{%
% Format chapter entries so that the chapter name goes on the same line
% as "Chapter #".
\renewcommand{\cftchappresnum}{Chapter }
\settowidth{\mylength}{\bfseries\cftchappresnum\cftchapaftersnum}% extra space
\addtolength{\cftchapnumwidth}{\mylength} % add the extra space
%
\newcommand{\mylongname}{Appendix }% the longest chapter number header
\settowidth{\mylonglength}{\bfseries\mylongname\cftchapaftersnum}% extra space
}
{%
\renewcommand{\cftchappresnum}{Chapter } 
\renewcommand{\cftchapaftersnumb}{\\ \mbox{}\hspace{-\mylength}\hspace{-0.1em}}
\settowidth{\mylength}{\bfseries\cftchappresnum\cftchapaftersnum} % extra space 
\addtolength{\cftchapnumwidth}{\mylength+0.1em} % add the extra space\renewcommand{\cftchapfont}{\bfseries} 
}%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Here we define internal "commands" that will be used to store the
% thesis title, author name, department, etc.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Store the title of the thesis.
\newcommand{\bits@title}{Theory of Everything}
\renewcommand{\title}[1]{\renewcommand{\bits@title}{#1}}

% Store the author's name.
\newcommand{\bits@author}{Ankit Dimri}
\renewcommand{\author}[1]{\renewcommand{\bits@author}{#1}}

% Store the author's name.
\newcommand{\bits@logo}{logo.png}
\providecommand{\logo}[1]{\renewcommand{\bits@logo}{#1}}

% Store the department name.
\newcommand{\bits@dept}{Design Engineering}
\providecommand{\dept}[1]{\renewcommand{\bits@dept}{#1}}

% Store the date the degree will be conferred.
\newcommand{\bits@degreedate}{June 2018}
\providecommand{\degreedate}[1]{\renewcommand{\bits@degreedate}{#1}}

% Store the year of the copyright.
\newcommand{\bits@copyrightyear}{2018}
\providecommand{\copyrightyear}[1]{\renewcommand{\bits@copyrightyear}{#1}}

% Store the document type.
\newcommand{\bits@documenttype}{Thesis}
\providecommand{\documenttype}[1]{\renewcommand{\bits@documenttype}{#1}}

% Store the academic unit to which the document has been submitted.
\newcommand{\bits@submittedto}{Design Engineering, Graduate School}
\providecommand{\submittedto}[1]{\renewcommand{\bits@submittedto}{#1}}

% Store the the info for the honors degree(s) type(s).
\newcommand{\bits@honorsdegreeinfo}{for a baccalaureate degree(s) \\ in Biology and Physics \\ with honors in Computer Engineering}
\providecommand{\honorsdegreeinfo}[1]{\renewcommand{\bits@honorsdegreeinfo}{#1}}

% Store the academic unit to which the document has been submitted.
\newcommand{\bits@honorsadviser}{Your Honors Adviser}
\providecommand{\honorsadviser}[1]{\renewcommand{\bits@honorsadviser}{#1}}

% Store the academic unit to which the document has been submitted.
\newcommand{\bits@honorsdepthead}{Department Q. Head}
\providecommand{\honorsdepthead}[1]{\renewcommand{\bits@honorsdepthead}{#1}}

% Store the name of the second Thesis Supervisor for a baccalaureate degree.
\newcommand{\bits@secondthesissupervisor}{Second Q. Jones}
\providecommand{\secondthesissupervisor}[1]{\renewcommand{\bits@secondthesissupervisor}{#1}}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Store the name of the degree by determining which boolean was
% set in the class option was specified.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\ifthenelse{\boolean{bits@bs}}%
{\newcommand{\bits@degreetype}{Baccalaureate of Science}}%
{}

\ifthenelse{\boolean{bits@ba}}%
{\newcommand{\bits@degreetype}{Baccalaureate of Arts}}%
{}

\ifthenelse{\boolean{bits@ms}}%
{\newcommand{\bits@degreetype}{Master of Science}}%
{}

\ifthenelse{\boolean{bits@meng}}%
{\newcommand{\bits@degreetype}{Master of Technology}}%
{}

\ifthenelse{\boolean{bits@ma}}%
{\newcommand{\bits@degreetype}{Master of Arts}}%
{}

\ifthenelse{\boolean{bits@phd}}%
{\newcommand{\bits@degreetype}{Doctor of Philosophy}}%
{}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Store the number of readers in \bits@readers. This quantity is
% input in the main file using the \numberofreaders command.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newcommand{\bits@readers}{4}
\providecommand{\numberofreaders}[1]{\renewcommand{\bits@readers}{#1}}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% 
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newcommand{\bits@advisor}{Elbert Jaypaul}
\newcommand{\bits@advisortitle}{Elbert Jaypaul}
\newcommand{\bits@advisoroption}{}%
\providecommand{\advisor}[3][]%
{\renewcommand{\bits@advisoroption}{#1}%
\renewcommand{\bits@advisor}{#2}%
\renewcommand{\bits@advisortitle}{#3}}

\newcommand{\bits@readerone}{Elbert Jaypaul}
\newcommand{\bits@readeronetitle}{Elbert Jaypaul}
\newcommand{\bits@readeroneoption}{}%
\providecommand{\readerone}[3][]%
{\renewcommand{\bits@readeroneoption}{#1}%
\renewcommand{\bits@readerone}{#2}%
\renewcommand{\bits@readeronetitle}{#3}}

\newcommand{\bits@readertwo}{Elbert Jaypaul}
\newcommand{\bits@readertwotitle}{Elbert Jaypaul}
\newcommand{\bits@readertwooption}{}%
\providecommand{\readertwo}[3][]%
{\renewcommand{\bits@readertwooption}{#1}%
\renewcommand{\bits@readertwo}{#2}%
\renewcommand{\bits@readertwotitle}{#3}}

\newcommand{\bits@readerthree}{Elbert Jaypaul}
\newcommand{\bits@readerthreetitle}{Elbert Jaypaul}
\newcommand{\bits@readerthreeoption}{}%
\providecommand{\readerthree}[3][]%
{\renewcommand{\bits@readerthreeoption}{#1}%
\renewcommand{\bits@readerthree}{#2}%
\renewcommand{\bits@readerthreetitle}{#3}}

\newcommand{\bits@readerfour}{Elbert Jaypaul}
\newcommand{\bits@readerfourtitle}{Elbert Jaypaul}
\newcommand{\bits@readerfouroption}{}%
\providecommand{\readerfour}[3][]%
{\renewcommand{\bits@readerfouroption}{#1}%
\renewcommand{\bits@readerfour}{#2}%
\renewcommand{\bits@readerfourtitle}{#3}}

\newcommand{\bits@readerfive}{Elbert Jaypaul}
\newcommand{\bits@readerfivetitle}{Elbert Jaypaul}
\newcommand{\bits@readerfiveoption}{}%
\providecommand{\readerfive}[3][]%
{\renewcommand{\bits@readerfiveoption}{#1}%
\renewcommand{\bits@readerfive}{#2}%
\renewcommand{\bits@readerfivetitle}{#3}}

\newcommand{\bits@readersix}{Elbert Jaypaul}
\newcommand{\bits@readersixtitle}{Elbert Jaypaul}
\newcommand{\bits@readersixoption}{}%
\providecommand{\readersix}[3][]%
{\renewcommand{\bits@readersixoption}{#1}%
\renewcommand{\bits@readersix}{#2}%
\renewcommand{\bits@readersixtitle}{#3}}


\newsavebox{\tempbox}
\renewcommand{\@makecaption}[2]{%
\vspace{7pt}\sbox{\tempbox}{\small\textbf{#1.} #2}%
\ifthenelse{\lengthtest{\wd\tempbox > \linewidth}}%
{\small\textbf{#1.} #2\par}%
{\centering \small\textbf{#1.} #2}%
}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                %
% The actual layout begins here. %
%                                %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Here is the permission page.
\newcommand{\bitspermissionpage}{%
\thispagestyle{empty}
\begin{singlespace}
\noindent
I grant the Birla Institute of Technology and Sciences, Pilani, India the non-exclusive right to use this work for the University's own purposes and to make single copies of the work available to the public on a not-for-profit basis if copies are not otherwise available. \\[0.6in]
\mbox{} \hfill
\parbox{3in}{\begin{center} \rule{3in}{0.4pt} \\ \bits@author
\end{center}}
\end{singlespace}
\newpage
\addtocounter{page}{-1}
}

% Here is the permission page.
\newcommand{\bitscertificatepage}{%
	\thispagestyle{empty}
	\begin{center}
	\textbf{CERTIFICATE}
	\end{center}
	\begin{singlespace}
		\noindent
		
		This is to certify that the Dissertation entitled \textbf{\bits@title} and  submitted by \textbf{\bits@author}  having  ID-No. \textbf{2016BA30045} for the partial fulfillment of the requirements of M.Tech. Design Engineering degree of BITS, embodies the bonafide work done by him under my supervision.
		 \\[0.6in]
		\mbox{} \hfill
		\parbox{3in}{\begin{center} \rule{3in}{0.4pt} \\ Signature of the Supervisor
		\end{center}}
	\end{singlespace}
	\newpage
	\addtocounter{page}{-1}
}


% Here is the title page.
\newcommand{\bitstitlepage}{%
\setcounter{page}{1}
\thispagestyle{empty}%
%%%
\ifthenelse{\boolean{bits@honors}}
%%%
{%
\vspace*{-0.75in}
%\enlargethispage{0.5in}
\begin{center}
	Birla Institute of Technology and Sciences
\end{center}
\vfill
\begin{center}
\bits@dept
\end{center}

\vfill
\begin{center}
\bits@title
\end{center}
\vfill
\begin{center}
\bits@author \\ \bits@degreedate
\end{center}
\vfill
\begin{center}
A thesis \\ submitted in partial fulfillment \\ of the requirements \\
\bits@honorsdegreeinfo
\end{center}
\vfill
\mbox{}
\ifthenelse{\boolean{bits@honorsdepthead}}{%
\begin{tabbing}%
Approved: \= \rule{2.75in}{0.5pt} \quad Date: \= \rule{1.5in}{0.5pt} \\[-3pt]
          \> \qquad \bits@advisor \\[-3pt]
          \> \qquad Thesis Supervisor \\[8mm]
%
\ifthenelse{\boolean{bits@secondthesissupervisor}}{%
          \> \rule{2.75in}{0.5pt}              \> \rule{1.5in}{0.5pt} \\[-3pt]
          \> \qquad \bits@secondthesissupervisor \\[-3pt]
          \> \qquad Thesis Supervisor \\[8mm]
}{}%
%
          \> \rule{2.75in}{0.5pt}              \> \rule{1.5in}{0.5pt} \\[-3pt]
          \> \qquad \bits@honorsadviser \\[-3pt]
          \> \qquad Honors Adviser \\[8mm]
 %
          \> \rule{2.75in}{0.5pt}              \> \rule{1.5in}{0.5pt} \\[-3pt]
          \> \qquad \bits@honorsdepthead \\[-3pt]
          \> \qquad Department Head
\end{tabbing}%
}%
{%
\begin{tabbing}%
Approved: \= \rule{2.75in}{0.5pt} \quad Date: \= \rule{1.5in}{0.5pt} \\[-3pt]
          \> \qquad \bits@advisor \\[-3pt]
          \> \qquad Thesis Supervisor \\[8mm]
%
\ifthenelse{\boolean{bits@secondthesissupervisor}}{%
          \> \rule{2.75in}{0.5pt}              \> \rule{1.5in}{0.5pt} \\[-3pt]
          \> \qquad \bits@secondthesissupervisor \\[-3pt]
          \> \qquad Thesis Supervisor \\[8mm]
}{}%
%
          \> \rule{2.75in}{0.5pt}              \> \rule{1.5in}{0.5pt} \\[-3pt]
          \> \qquad \bits@honorsadviser \\[-3pt]
          \> \qquad Honors Adviser
\end{tabbing}%
}%
}%
%%%
{%
\vspace*{-0.75in}
\begin{center}
	 \includegraphics[width=35mm]{\bits@logo}\\
    \textbf{Birla Institute of Technology and Sciences\\
            \bits@submittedto} 
\end{center}
\vfill
\begin{center}
    \setstretch{2}
    \bfseries\uppercase\expandafter{\bits@title}
\end{center}
\vfill
\begin{center}
    A \bits@documenttype\ in\\
    \bits@dept\\
    by\\
    \bits@author\\
\end{center}
\vfill
\begin{center}
	Dissertation work carried out at \\
	\textbf{Rolls-Royce plc, Bangalore, India}
\end{center}
\vfill
\begin{center}
	\copyright\ \bits@copyrightyear\ \bits@author
\end{center}
\vfill
\begin{center}
    Submitted in Partial Fulfillment\\
    of the Requirements\\
    for the Degree of
\end{center}
\vfill
\begin{center}
     \bits@degreetype
\end{center}
\vfill
\begin{center}
    \bits@degreedate
\end{center}
\newpage
\ifthenelse{\boolean{bits@ms} \or \boolean{bits@meng} \or \boolean{bits@ma}}{\bitspermissionpage}{}
}
%%%
}


% Here is the signature page.
\newcommand{\signature}[1]{%
\begin{minipage}{\textwidth}
   \rule{3.9in}{0.4pt} \hfill \rule{1.375in}{0.4pt} \\ {\normalsize #1}
\end{minipage}}

\newlength{\bits@sigoptionskip}
\newlength{\bits@sigafteroptionskip}
\newlength{\bits@intersigspace}
% \newlength{\bits@spacebeforedateofsig}
% \settowidth{\bits@spacebeforedateofsig}{\normalsize Date of Signature}
\setlength{\bits@sigoptionskip}{-0.04in}
\newcommand{\bits@signaturepage}{%
    \footnotesize
    \ifthenelse{\bits@readers = 6}{%
        \setlength{\bits@sigafteroptionskip}{2.65\baselineskip}
        \setlength{\bits@intersigspace}{2.65\baselineskip}
    }%
    {%
        \setlength{\bits@sigafteroptionskip}{3.4\baselineskip}
        \setlength{\bits@intersigspace}{3.4\baselineskip}
    }
    \enlargethispage*{0.5in}
    \thispagestyle{empty}
    \vspace*{-0.5in}
    \noindent {\normalsize We approve the thesis of \bits@author.}\\[0.1in]
    \mbox{}\hspace{4.37in} {\normalsize Date of Signature}\\[3\baselineskip]
    \signature\bits@advisor\\[1mm]
         \bits@advisortitle
         \ifthenelse{\equal{\bits@advisoroption}{}}%
                    {\\[\bits@intersigspace]}%
                    {\\[\bits@sigoptionskip]
                        \bits@advisoroption \\[\bits@sigafteroptionskip]}
\ifcase \bits@readers
\or
     \signature\bits@readerone\\[1mm]
          \bits@readeronetitle
          \ifthenelse{\equal{\bits@readeroneoption}{}}%
                     {}%
                     {\\[\bits@sigoptionskip] \bits@readeroneoption}
\or
     \signature\bits@readerone\\[1mm]
          \bits@readeronetitle
          \ifthenelse{\equal{\bits@readeroneoption}{}}%
                     {\\[\bits@intersigspace]}%
                     {\\[\bits@sigoptionskip]
                               \bits@readeroneoption \\[\bits@sigafteroptionskip]}
     \signature\bits@readertwo\\[1mm]
          \bits@readertwotitle
          \ifthenelse{\equal{\bits@readertwooption}{}}%
                     {}%
                     {\\[\bits@sigoptionskip] \bits@readertwooption}
\or
     \signature{\bits@readerone}\\[1mm]
          \bits@readeronetitle
          \ifthenelse{\equal{\bits@readeroneoption}{}}%
                     {\\[\bits@intersigspace]}%
                     {\\[\bits@sigoptionskip]
                               \bits@readeroneoption \\[\bits@sigafteroptionskip]}
     \signature{\bits@readertwo}\\[1mm]
          \bits@readertwotitle
          \ifthenelse{\equal{\bits@readertwooption}{}}%
                     {\\[\bits@intersigspace]}%
                     {\\[\bits@sigoptionskip]
                               \bits@readertwooption \\[\bits@sigafteroptionskip]}
     \signature{\bits@readerthree}\\[1mm]
          \bits@readerthreetitle
          \ifthenelse{\equal{\bits@readerthreeoption}{}}%
                     {}%
                     {\\[\bits@sigoptionskip] \bits@readerthreeoption}
\or
     \signature\bits@readerone\\[1mm]
          \bits@readeronetitle
          \ifthenelse{\equal{\bits@readeroneoption}{}}%
                     {\\[\bits@intersigspace]}%
                     {\\[\bits@sigoptionskip]
                               \bits@readeroneoption \\[\bits@sigafteroptionskip]}
     \signature\bits@readertwo\\[1mm]
          \bits@readertwotitle
          \ifthenelse{\equal{\bits@readertwooption}{}}%
                     {\\[\bits@intersigspace]}%
                     {\\[\bits@sigoptionskip]
                               \bits@readertwooption \\[\bits@sigafteroptionskip]}
     \signature\bits@readerthree\\[1mm]
          \bits@readerthreetitle
          \ifthenelse{\equal{\bits@readerthreeoption}{}}%
                     {\\[\bits@intersigspace]}%
                     {\\[\bits@sigoptionskip]
                               \bits@readerthreeoption \\[\bits@sigafteroptionskip]}
     \signature\bits@readerfour\\[1mm]
          \bits@readerfourtitle
          \ifthenelse{\equal{\bits@readerfouroption}{}}%
                     {}%
                     {\\[\bits@sigoptionskip] \bits@readerfouroption}
\or
     \signature\bits@readerone\\[1mm]
          \bits@readeronetitle
          \ifthenelse{\equal{\bits@readeroneoption}{}}%
                     {\\[\bits@intersigspace]}%
                     {\\[\bits@sigoptionskip]
                               \bits@readeroneoption \\[\bits@sigafteroptionskip]}
     \signature\bits@readertwo\\[1mm]
          \bits@readertwotitle
          \ifthenelse{\equal{\bits@readertwooption}{}}%
                     {\\[\bits@intersigspace]}%
                     {\\[\bits@sigoptionskip]
                               \bits@readertwooption \\[\bits@sigafteroptionskip]}
     \signature\bits@readerthree\\[1mm]
          \bits@readerthreetitle
          \ifthenelse{\equal{\bits@readerthreeoption}{}}%
                     {\\[\bits@intersigspace]}%
                     {\\[\bits@sigoptionskip]
                               \bits@readerthreeoption \\[\bits@sigafteroptionskip]}
     \signature\bits@readerfour\\[1mm]
          \bits@readerfourtitle
          \ifthenelse{\equal{\bits@readerfouroption}{}}%
                     {\\[\bits@intersigspace]}%
                     {\\[\bits@sigoptionskip]
                               \bits@readerfouroption \\[\bits@sigafteroptionskip]}
     \signature\bits@readerfive\\[1mm]
          \bits@readerfivetitle
          \ifthenelse{\equal{\bits@readerfiveoption}{}}%
                     {}%
                     {\\[\bits@sigoptionskip] \bits@readerfiveoption}
\or
     \signature\bits@readerone\\[1mm]
          \bits@readeronetitle
          \ifthenelse{\equal{\bits@readeroneoption}{}}%
                     {\\[\bits@intersigspace]}%
                     {\\[\bits@sigoptionskip]
                               \bits@readeroneoption \\[\bits@sigafteroptionskip]}
     \signature\bits@readertwo\\[1mm]
          \bits@readertwotitle
          \ifthenelse{\equal{\bits@readertwooption}{}}%
                     {\\[\bits@intersigspace]}%
                     {\\[\bits@sigoptionskip]
                               \bits@readertwooption \\[\bits@sigafteroptionskip]}
     \signature\bits@readerthree\\[1mm]
          \bits@readerthreetitle
          \ifthenelse{\equal{\bits@readerthreeoption}{}}%
                     {\\[\bits@intersigspace]}%
                     {\\[\bits@sigoptionskip]
                               \bits@readerthreeoption \\[\bits@sigafteroptionskip]}
     \signature\bits@readerfour\\[1mm]
          \bits@readerfourtitle
          \ifthenelse{\equal{\bits@readerfouroption}{}}%
                     {\\[\bits@intersigspace]}%
                     {\\[\bits@sigoptionskip]
                               \bits@readerfouroption \\[\bits@sigafteroptionskip]}
     \signature\bits@readerfive\\[1mm]
          \bits@readerfivetitle
          \ifthenelse{\equal{\bits@readerfiveoption}{}}%
                     {\\[\bits@intersigspace]}%
                     {\\[\bits@sigoptionskip]
                               \bits@readerfiveoption \\[\bits@sigafteroptionskip]}
     \signature\bits@readersix\\[1mm]
          \bits@readersixtitle
          \ifthenelse{\equal{\bits@readersixoption}{}}%
                     {}%
                     {\\[\bits@sigoptionskip] \bits@readersixoption}
\fi
\newpage
\normalsize
}



% Here is the committee page.

\newcommand{\bitscommitteepage}{%
    \setlength{\textheight}{8in}
    \ifthenelse{\bits@readers = 6}{%
        \setlength{\bits@sigafteroptionskip}{1.0\baselineskip}
        \setlength{\bits@intersigspace}{1.0\baselineskip}
    }%
    {%
        \setlength{\bits@sigafteroptionskip}{1.5\baselineskip}
        \setlength{\bits@intersigspace}{1.5\baselineskip}
    }
    \thispagestyle{empty}
    \vspace*{-0.5in}
    \noindent {\normalsize The thesis of \bits@author\ was reviewed and approved$^{*}$ by the following:}\\[3\baselineskip]
\mbox{}\hfill
\parbox{\textwidth - 0.5in}{
         \bits@advisor\\[\bits@sigoptionskip]
         \bits@advisortitle
         \ifthenelse{\equal{\bits@advisoroption}{}}%
                    {\\[\bits@intersigspace]}%
                    {\\[\bits@sigoptionskip]
                        \bits@advisoroption \\[\bits@sigafteroptionskip]}
\ifcase \bits@readers
\or
          \bits@readerone\\[\bits@sigoptionskip]
          \bits@readeronetitle
          \ifthenelse{\equal{\bits@readeroneoption}{}}%
                     {}%
                     {\\[\bits@sigoptionskip] \bits@readeroneoption}
\or
          \bits@readerone\\[\bits@sigoptionskip]
          \bits@readeronetitle
          \ifthenelse{\equal{\bits@readeroneoption}{}}%
                     {\\[\bits@intersigspace]}%
                     {\\[\bits@sigoptionskip]
                               \bits@readeroneoption \\[\bits@sigafteroptionskip]}
          \bits@readertwo\\[\bits@sigoptionskip]
          \bits@readertwotitle
          \ifthenelse{\equal{\bits@readertwooption}{}}%
                     {}%
                     {\\[\bits@sigoptionskip] \bits@readertwooption}
\or
          {\bits@readerone}\\[\bits@sigoptionskip]
          \bits@readeronetitle
          \ifthenelse{\equal{\bits@readeroneoption}{}}%
                     {\\[\bits@intersigspace]}%
                     {\\[\bits@sigoptionskip]
                               \bits@readeroneoption \\[\bits@sigafteroptionskip]}
          {\bits@readertwo}\\[\bits@sigoptionskip]
          \bits@readertwotitle
          \ifthenelse{\equal{\bits@readertwooption}{}}%
                     {\\[\bits@intersigspace]}%
                     {\\[\bits@sigoptionskip]
                               \bits@readertwooption \\[\bits@sigafteroptionskip]}
          {\bits@readerthree}\\[\bits@sigoptionskip]
          \bits@readerthreetitle
          \ifthenelse{\equal{\bits@readerthreeoption}{}}%
                     {}%
                     {\\[\bits@sigoptionskip] \bits@readerthreeoption}
\or
          \bits@readerone\\[\bits@sigoptionskip]
          \bits@readeronetitle
          \ifthenelse{\equal{\bits@readeroneoption}{}}%
                     {\\[\bits@intersigspace]}%
                     {\\[\bits@sigoptionskip]
                               \bits@readeroneoption \\[\bits@sigafteroptionskip]}
          \bits@readertwo\\[\bits@sigoptionskip]
          \bits@readertwotitle
          \ifthenelse{\equal{\bits@readertwooption}{}}%
                     {\\[\bits@intersigspace]}%
                     {\\[\bits@sigoptionskip]
                               \bits@readertwooption \\[\bits@sigafteroptionskip]}
          \bits@readerthree\\[\bits@sigoptionskip]
          \bits@readerthreetitle
          \ifthenelse{\equal{\bits@readerthreeoption}{}}%
                     {\\[\bits@intersigspace]}%
                     {\\[\bits@sigoptionskip]
                               \bits@readerthreeoption \\[\bits@sigafteroptionskip]}
          \bits@readerfour\\[\bits@sigoptionskip]
          \bits@readerfourtitle
          \ifthenelse{\equal{\bits@readerfouroption}{}}%
                     {}%
                     {\\[\bits@sigoptionskip] \bits@readerfouroption}
\or
          \bits@readerone\\[\bits@sigoptionskip]
          \bits@readeronetitle
          \ifthenelse{\equal{\bits@readeroneoption}{}}%
                     {\\[\bits@intersigspace]}%
                     {\\[\bits@sigoptionskip]
                               \bits@readeroneoption \\[\bits@sigafteroptionskip]}
          \bits@readertwo\\[\bits@sigoptionskip]
          \bits@readertwotitle
          \ifthenelse{\equal{\bits@readertwooption}{}}%
                     {\\[\bits@intersigspace]}%
                     {\\[\bits@sigoptionskip]
                               \bits@readertwooption \\[\bits@sigafteroptionskip]}
          \bits@readerthree\\[\bits@sigoptionskip]
          \bits@readerthreetitle
          \ifthenelse{\equal{\bits@readerthreeoption}{}}%
                     {\\[\bits@intersigspace]}%
                     {\\[\bits@sigoptionskip]
                               \bits@readerthreeoption \\[\bits@sigafteroptionskip]}
          \bits@readerfour\\[\bits@sigoptionskip]
          \bits@readerfourtitle
          \ifthenelse{\equal{\bits@readerfouroption}{}}%
                     {\\[\bits@intersigspace]}%
                     {\\[\bits@sigoptionskip]
                               \bits@readerfouroption \\[\bits@sigafteroptionskip]}
          \bits@readerfive\\[\bits@sigoptionskip]
          \bits@readerfivetitle
          \ifthenelse{\equal{\bits@readerfiveoption}{}}%
                     {}%
                     {\\[\bits@sigoptionskip] \bits@readerfiveoption}
\or
          \bits@readerone\\[\bits@sigoptionskip]
          \bits@readeronetitle
          \ifthenelse{\equal{\bits@readeroneoption}{}}%
                     {\\[\bits@intersigspace]}%
                     {\\[\bits@sigoptionskip]
                               \bits@readeroneoption \\[\bits@sigafteroptionskip]}
          \bits@readertwo\\[\bits@sigoptionskip]
          \bits@readertwotitle
          \ifthenelse{\equal{\bits@readertwooption}{}}%
                     {\\[\bits@intersigspace]}%
                     {\\[\bits@sigoptionskip]
                               \bits@readertwooption \\[\bits@sigafteroptionskip]}
          \bits@readerthree\\[\bits@sigoptionskip]
          \bits@readerthreetitle
          \ifthenelse{\equal{\bits@readerthreeoption}{}}%
                     {\\[\bits@intersigspace]}%
                     {\\[\bits@sigoptionskip]
                               \bits@readerthreeoption \\[\bits@sigafteroptionskip]}
          \bits@readerfour\\[\bits@sigoptionskip]
          \bits@readerfourtitle
          \ifthenelse{\equal{\bits@readerfouroption}{}}%
                     {\\[\bits@intersigspace]}%
                     {\\[\bits@sigoptionskip]
                               \bits@readerfouroption \\[\bits@sigafteroptionskip]}
          \bits@readerfive\\[\bits@sigoptionskip]
          \bits@readerfivetitle
          \ifthenelse{\equal{\bits@readerfiveoption}{}}%
                     {\\[\bits@intersigspace]}%
                     {\\[\bits@sigoptionskip]
                               \bits@readerfiveoption \\[\bits@sigafteroptionskip]}
          \bits@readersix\\[\bits@sigoptionskip]
          \bits@readersixtitle
          \ifthenelse{\equal{\bits@readersixoption}{}}%
                     {}%
                     {\\[\bits@sigoptionskip] \bits@readersixoption}
\fi
}

\mbox{}\vfill

\noindent
\parbox{\textwidth}{$^{*}$Signatures are on file in the Graduate School.}

\newpage
}
 



\newcommand{\bitssigpage}{%\setlength{\textheight}{8.0in}
{\Huge\thispagestyle{empty}
\sffamily\bfseries
\mbox{}\vfill
\noindent
The following page is the Signature Page. The Signature Page needs to be given to the Grad School, but it should not be bound with the thesis.
\begin{center}
\tiny Don't bind this page either!
\end{center}
\vfill
}
\newpage 
\bits@signaturepage}



\newcommand{\thesisabstract}[1]{%
\pagestyle{plain}
\chapter*{Abstract}
    \begin{singlespace}
        \input{#1}
    \end{singlespace}
\newpage
}


\renewcommand{\contentsname}{Table of Contents}
\setcounter{secnumdepth}{10}
\setcounter{tocdepth}{4}
\newcommand{\thesistableofcontents}{%
\begin{singlespace}
\if@twocolumn
\@restonecoltrue\onecolumn
\else
\@restonecolfalse
\fi
\chapter*{\contentsname
\@mkboth{%
\MakeUppercase\contentsname}{\MakeUppercase\contentsname}}%
\@starttoc{toc}%
\if@restonecol\twocolumn\fi%
\end{singlespace}
\clearpage
}

\renewcommand{\listfigurename}{List of Figures}
\newcommand{\thesislistoffigures}{%
\begin{singlespace}
\if@twocolumn
\@restonecoltrue\onecolumn
\else
\@restonecolfalse
\fi
\chapter{\listfigurename
\@mkboth{%
\MakeUppercase\listfigurename}{\MakeUppercase\listfigurename}}%
\@starttoc{lof}%
\if@restonecol\twocolumn\fi
\end{singlespace}
\clearpage
}

\renewcommand{\listtablename}{List of Tables}
\newcommand{\thesislistoftables}{%
\begin{singlespace}
\if@twocolumn
\@restonecoltrue\onecolumn
\else
\@restonecolfalse
\fi
\chapter{\listtablename
\@mkboth{%
\MakeUppercase\listtablename}{\MakeUppercase\listtablename}}%
\@starttoc{lot}%
\if@restonecol\twocolumn\fi
\end{singlespace}
\clearpage
}


\newcommand{\thesislistofsymbols}[1]{%
\chapter{List of Symbols}
%\addcontentsline{toc}{chapter}{List of Symbols}
\begin{singlespace}
    \input{#1}
\end{singlespace}
}



\newcommand{\thesisacknowledgments}[1]{%
\chapter{Acknowledgments}
%\addcontentsline{toc}{chapter}{Acknowledgments}
\begin{singlespace}
    \input{#1}
\end{singlespace}
}


\newcommand{\thesisdedication}[2]{%
\chapter*{#2}
\begin{singlespace}
    \input{#1}
\end{singlespace}
}

\newcommand{\thesisqoutes}[2]{%
\chapter*{#2}
\begin{singlespace}
	\begin{center}
    		\input{#1}
	\end{center}
\end{singlespace}
}




\newcommand{\Appendix}[1]{%
\ifthenelse{\value{chapter} = 0}
{
\ifthenelse{\boolean{bits@toc}}
{%
\addtocontents{toc}{\protect\addtolength{\cftchapnumwidth}{-\mylength}}
\addtocontents{toc}{\string\renewcommand{\string\cftchappresnum}{Appendix }}
\addtocontents{toc}{\protect\addtolength{\cftchapnumwidth}{\mylonglength}}
}%
{%
\addtocontents{toc}{\string\renewcommand{\string\cftchappresnum}{Appendix }}
}}{}%
\chapter{#1}
}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\DeclareRobustCommand{\thesismainmatter}{%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\setlength{\textheight}{8.5in}
\mainmatter
\pagestyle{empty}
\renewcommand{\@oddhead}{\mbox{}\hfill\arabic{page}}
\let\ps@plain = \ps@empty
}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\newcommand{\vita}[1]{%
\begin{singlespace}
\thispagestyle{empty}
\begin{center}
    \textbf{{\large Vita} \\[0.1in] \bits@author}
\end{center}
\input{#1}
\end{singlespace}
}


%%
%% End of file `bitsthesis.cls'.
